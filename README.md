# NodeJS Template
This project contains a set of files I like to re-use in a variety of NodeJS applications. This acts as a common place to track these files rather than keeping many copies all up to date with any changes.

# Using
A example-package.json file is provided as an example package.json file for any project that uses this template. Note that the
reference to the git repo may need changed based on your environment.

# License
This repo is licensed under the [Apache 2.0 License](https://apache.org/licenses/LICENSE-2.0).
